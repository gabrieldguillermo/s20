// S 20 - Javascript Repetition Control Structures

// ACTIVITY


/*
PART 1: 

	- Create a variable number that will store the value of the number provided by the user via the prompt.

	- Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

	- Create a condition that if the current value is less than or equal to 50, stop the loop.

	- Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

	- Create another condition that if the current value is divisible by 5, print the number.

*/

// CODE HERE:

console.log("%cActivity Part 1: ","color:red");
let myNumber = prompt("Enter a number");

console.log("The number you provide: " + myNumber);
for (let i = myNumber; i >= 0; i--) {

    if (i <= 50) {
        console.log("The current value is less than or equal to 50. Terminating the loop.");
        break;
     }

    if (i % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number");
continue;
    }

    if (i % 5 === 0) {
        console.log(i);
        
    }


}




/*
PART 2:

	- Create a varaible that will contain the string "supercalifragilisticexpialidocious"

	- Create another varaible that will store the consonants from the string.

	- Create a for loop that will iterate through the individual letters of the string based on it's length.

	- Create an if statement that will check if the individual letters of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

	- Create an else statement that will add the consonants to the second variable.


*/

// CODE HERE:
console.log("%cActivity Part 2: ","color:yellow");
let string = 'supercalifragilisticexpialidocious';

let consonants = '';

for (let char = 0; char < string.length; char++) {
    if (
        string[char].toLowerCase() == "a" ||
        string[char].toLowerCase() == "e" ||
        string[char].toLowerCase() == "i" ||
        string[char].toLowerCase() == "o" ||
        string[char].toLowerCase() == "u"
    ) {
        continue;
    } else {
        consonants += string[char];
    }
}
console.log(consonants);