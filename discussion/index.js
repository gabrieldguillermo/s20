// console.log("hello");




// let numA = 0;
// while (numA <= 30){
// 	console.log("While: " + numA);
// 	numA+=2;

// };


/*
 Do while loop
 -a DOo while loop works a lot like the while loop. But unlike while loops, do while-loops guarantee that the code will be executed at least once.


	syntax:
	do{
		statment
	} while (expression/condition){
	 	statement	
	}

*/

// let number = Number(prompt("Give me a number"));

// 	do {
// 		console.log("Do while:" + number);
// 	number++;
// 	} while (number < 10);



// let number2 = Number(prompt("Give me a number"));

// 	do {
// 		console.log("Do while:" + number2);
// 		number--;
// 	} while (number2 > 10);


	/*
	 For loop
	 	-more flexible than while loop and Do-while loop

	 	-part:
	 		- initial value: tracks the progress of the loop.
	 		- condition: if true it will run the code
	 		if false it will stop the iteration/code.
	 		- iteration 
	*/


	// for(let count = 19; count <= 20; count++){
	// 	console.log("For loop count: " + count);
	// }

	// let myString = "Gabby";

	// 	for(let i = 0; i < myString.length; i++){

	// 		console.log(myString[i]);
	// 	}


	// let myName = "JOSHUA"; 

	// for(let n = 0; n < myName.length; n++){

	// if(
	// 	myName[n].toLowerCase() == "a" ||
	// 	myName[n].toLowerCase() == "e" ||
	// 	myName[n].toLowerCase() == "i" ||
	// 	myName[n].toLowerCase() == "o" ||
	// 	myName[n].toLowerCase() == "u" 
	// 	){
	// 	console.log("Vowel");

	// 	}	else{

	// 		console.log(myName[n].toLowerCase());
	// 	}
	// }




let string = "extravagant";
let consonant='';
	
	//my answer 

	// for(let char = 0; char < string.length; char++){
	// 	if(
	// 		string[char].toLowerCase() !== "a" &&
	// 		string[char].toLowerCase() !== "e" &&
	// 		string[char].toLowerCase() !== "i" &&
	// 		string[char].toLowerCase() !== "o" &&
	// 		string[char].toLowerCase() !== "u" 
	// 	){
	// 		consonant += string[char];	
	// 	}
	// }
	// console.log(consonant);


	// pratice

// const  getConsonant = string => {
// 	  let vowels = /[^aeiou]/gi;
//   	  let result = string.match(vowels);
//   		consonant+=result;

//   console.log(consonant);
// };
// getConsonant(string);